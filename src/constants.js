export const BOT_USERNAME = process.env.NODE_ENV === 'production' ? 'bakatrouble_pw_bot' : 'bakatrouble_testbot';
export const API_DOMAIN = process.env.NODE_ENV === 'production' ? 'apps.bakatrouble.pw' : 'home.bakatrouble.pw';
