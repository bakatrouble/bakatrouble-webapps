// import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import 'semantic-ui-forest-themes/semantic.superhero.min.css';
import './app.css';

import Root from './routes';

const root = document.getElementById('root');

render((
  <AppContainer>
    <Root/>
  </AppContainer>), root);

if (module.hot) module.hot.accept();
