import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Comment } from 'semantic-ui-react';

const avatarStyle = {
  width: 64,
  height: 64,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center',
  backgroundSize: 'contain',
  marginRight: '1em',
};

const commentStyle = {
  paddingTop: '1em',
  marginTop: '1em',
  borderTop: '1px solid lightgrey',
};

class ArticleComment extends Component {
  render() {
    const _ = this.props.comment;
    return (
      <Comment key={`${_.id}`}>
        <Comment.Avatar src={_.userpic} />
        <Comment.Content>
          <Comment.Author>{_.displayName.trim() || '[No name]'}</Comment.Author>
          <Comment.Metadata>{_.createdAt}</Comment.Metadata>
          <Comment.Text dangerouslySetInnerHTML={{__html: _.html}} />
        </Comment.Content>
        { _.children ?
          <Comment.Group>{_.children.map(_ => <ArticleComment key={`${_.id}`} comment={_} />)}</Comment.Group> : ''}
      </Comment>
    );
  }

  static propTypes = {
    comment: PropTypes.object.isRequired,
  }
}

export default ArticleComment;
