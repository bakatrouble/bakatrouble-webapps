import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Menu } from 'semantic-ui-react';

class Sidebar extends Component {
  render() {
    return (
      <div>
        <h4>Apps list</h4>
        <Menu vertical>
          <Menu.Item as={NavLink} to='/lenta-comments' activeClassName='active'>Lenta comments</Menu.Item>
          <Menu.Item as={NavLink} to='/youtube-utils' activeClassName='active'>YouTube Utils</Menu.Item>
        </Menu>
      </div>
    );
  }

  static propTypes = {
    active: PropTypes.string
  }
}

export default Sidebar;
