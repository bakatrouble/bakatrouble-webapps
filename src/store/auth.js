import { observable, action, computed } from 'mobx';
import axios from 'axios';
import utf8 from 'utf8';
import store from 'store2';

import { API_DOMAIN } from '../constants';

class AuthStore {
  @observable userData = null;
  @observable permissions = [];

  @action login(user) {
    this.getAxios(user).get('/me/')
      .then(() => {
        this.userData = user;
        store.set('userData', user);
      }).catch(response => {
        this.userData = null;
        store.remove('userData');
        try {
          alert(response.response.data.detail);
        } catch (e) {
          alert('An error has occured, please try later');
        }
      });
  }

  startLogin() {
    const userData = store.get('userData');
    if (userData) {
      this.login(userData);
    }
  }

  getAxios(userData = null) {
    const data = btoa(utf8.encode(JSON.stringify(userData || this.userData)));
    return axios.create({
      baseURL: `https://${API_DOMAIN}/_api/`,
      headers: {
        'Authorization': `Telegram ${data}`
      }
    });
  }
}

export default AuthStore;
