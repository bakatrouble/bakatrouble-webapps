import { observable, action } from 'mobx';
import axios from 'axios';

const APP_ID = 126;

class LentaCommentsStore {
  @observable comments = [];
  @observable loading = false;
  @observable error = false;

  @action.bound
  loadComments(xid) {
    this.comments = [];
    this.loading = true;
    this.error = false;

    axios.get(`https://c.rambler.ru/api/app/${APP_ID}/widget/init/`, {
      params: { appId: APP_ID, xid: xid }
    }).then(data => {
      data = data.data;
      if (typeof data.comments === 'undefined') throw new Error();

      this.comments = data.comments;
      this.loading = false;
      this.error = false;
    }).catch(() => {
      this.comments = [];
      this.loading = false;
      this.error = true;
    });
  }
}

export default LentaCommentsStore;
