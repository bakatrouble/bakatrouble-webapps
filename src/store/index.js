import LentaCommentsStore from './lentaComments';
import AuthStore from './auth';

export const lentaCommentsStore = new LentaCommentsStore();
export const authStore = new AuthStore();
