import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Form, Input, Button, Message, CommentGroup, Icon } from 'semantic-ui-react';
import YouTube from 'react-youtube';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import qs from 'query-string';

import './style.css';

function YouTubeGetID(url){
  let ID = null;
  url = url.replace(/([<>])/gi,'');
  const parts = url.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if(parts.length > 2) {
    ID = parts[2].split(/[^0-9a-z_-]/i)[0];
  } else if (url.search(/[^0-9a-zA-Z_-]/) === -1) {
    ID = url;
  }
  return ID;
}

function roundTime(value) {
  return Math.round(value * 10) / 10;
}

@observer
class YouTubeUtils extends Component {
  player = null;
  timerInterval = null;
  state = {
    videoId: '',
    duration: 0,
    timeRange: { min: 0, max: 0 },
    videoRange: { min: 0, max: 0 },
    showPlayer: false,
    videoReady: false
  };

  onVideoReady(e) {
    this.player = e.target;
    const duration = this.player.getDuration();
    const params = qs.parse(this.props.location.search);
    const fromTime = Math.max(Number(params.from), 0) || 0;
    const toTime = Math.min(Number(params.to), duration) || duration;
    const range = { min: fromTime, max: toTime };
    this.props.history.replace(`?videoId=${this.state.videoId}&from=${fromTime}&to=${toTime}`);
    this.setState({
      videoReady: true,
      duration,
      timeRange: range,
      videoRange: range
    });
  }

  timerCallback() {
    if (!this.player) return;
    const currentTime = this.player.getCurrentTime();
    if (currentTime + 1 < this.state.videoRange.min ||
        currentTime - 1 > this.state.videoRange.max ||
        [
          YouTube.PlayerState.UNSTARTED,
          YouTube.PlayerState.ENDED,
          YouTube.PlayerState.CUED
        ].includes(this.player.getPlayerState())) {
      this.player.seekTo(this.state.videoRange.min, true);
      this.player.playVideo();
    }
  }

  componentDidMount() {
    const params = qs.parse(this.props.location.search);
    if (params.videoId) {
      this.setState({videoId: params.videoId, showPlayer: true});
      this.linkInput.inputRef.value = `https://youtu.be/${params.videoId}`;
    }
    this.timerInterval = setInterval(::this.timerCallback, 100);
  }

  onSubmit() {
    const videoId = YouTubeGetID(this.linkInput.inputRef.value);
    if (videoId === this.state.videoId) return;
    if (videoId === null) console.log('error');
    this.props.history.replace(`?videoId=${videoId}`);
    this.setState({ videoId, showPlayer: true, videoReady: false, duration: 0 });
  }

  onRangeChangeComplete(value) {
    this.setState({ videoRange: value });
    this.props.history.replace(`?videoId=${this.state.videoId}&from=${value.min}&to=${value.max}`);
  }

  setStartTimeToCurrent() {
    const currentTime = roundTime(this.player.getCurrentTime());
    const range = { min: currentTime, max: Math.max(this.state.videoRange.max, currentTime + 1) };
    this.setState({ timeRange: range });
    this.onRangeChangeComplete(range);
  }

  setEndTimeToCurrent() {
    const currentTime = roundTime(this.player.getCurrentTime());
    const range = { min: Math.min(this.state.videoRange.min, currentTime - 1), max: currentTime };
    this.setState({ timeRange: range });
    this.onRangeChangeComplete(range);
  }

  render() {
    const opts = {
      width: '100%',
      height: '100%',
      playerVars: {
        autoplay: 1,
        loop: 1,
        rel: 0
      }
    };
    return (
      <div>
        <Form onSubmit={::this.onSubmit} style={{ marginBottom: '2em' }}>
          <Input
            fluid
            placeholder='Video link'
            ref={input => this.linkInput = input}
            action={<Button type='submit' content='Load' />} />
        </Form>
        { this.state.showPlayer ?
          <div>
            <div className='player-wrapper'>
              <YouTube className='player' videoId={this.state.videoId} opts={opts} onReady={ ::this.onVideoReady } />
            </div>
            { this.state.videoReady ?
              <div>
                <InputRange
                  className='range-slider'
                  step={.1}
                  minValue={0}
                  maxValue={this.state.duration}
                  formatLabel={ value => `${roundTime(value)} s` }
                  value={this.state.timeRange}
                  onChange={ value => this.setState({ timeRange: value }) }
                  onChangeComplete={ ::this.onRangeChangeComplete } />
                <Button onClick={ ::this.setStartTimeToCurrent }>Start with current time</Button>
                <Button onClick={ ::this.setEndTimeToCurrent } floated='right'>End with current time</Button>
              </div> : '' }
          </div> : <Message info>Please input video link.</Message> }
      </div>
    );
  }
}

export default YouTubeUtils;
