import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Form, Input, Button, Message, CommentGroup, Icon } from 'semantic-ui-react';

import { lentaCommentsStore } from '../../store';
import Comment from '../../components/lentaComments/Comment';

@observer
class LentaComments extends Component {
  onSubmit(e) {
    lentaCommentsStore.loadComments(this.linkInput.inputRef.value);
    e.preventDefault();
  }

  getComments(comments) {
    const commentsList = [];
    const commentsFlatMap = {};
    for (const comment of comments) {
      comment.children = [];
      commentsFlatMap[comment.id] = comment;
      if (comment.parentId === null) {
        commentsList.push(comment);
      } else {
        commentsFlatMap[comment.parentId].children.push(comment);
      }
    }
    return commentsList.map(_ => <Comment key={`${_.id}`} comment={_} />);
  }

  render() {
    const { comments, loading, error } = lentaCommentsStore;

    return (
      <div>
        <h1>Lenta.ru comments viewer</h1>
        <p>Using this viewer you could read even closed article comments.</p>
        <Form onSubmit={::this.onSubmit} style={{ marginBottom: '2em' }}>
          <Input fluid placeholder='Article link' ref={input => this.linkInput = input}
            action={<Button content='Load' loading={loading} />} />
        </Form>
        {
          error
            ? <Message error>An error has occurred.</Message>
            : loading
              ? <Message info><Icon loading name='spinner' /> Loading...</Message>
              : !comments.length
                ? <Message info>Please input article link.</Message>
                : <CommentGroup>{ this.getComments(comments) }</CommentGroup>
        }
      </div>
    );
  }
}

export default LentaComments;
