import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Menu, Container, Grid, Modal } from 'semantic-ui-react';
import { observer } from 'mobx-react';

import TelegramLoginButton from '../components/TelegramLoginButton';
import { BOT_USERNAME } from '../constants';
import Index from './Index';
import LentaComments from './LentaComments';
import YouTubeUtils from './YouTubeUtils';
import Sidebar from '../components/Sidebar';
import { authStore } from '../store';

@observer
class Root extends Component {
  state = { loginModalIsOpen: false };

  toggleLoginModal = () => this.setState({ loginModalIsOpen: !this.state.loginModalIsOpen });

  componentDidMount() {
    authStore.startLogin();
  }

  render() {
    return (
      <Router>
        <div>
          <Menu fixed='top'>
            <Container>
              <Menu.Item header as={ Link } to='/'>bakatrouble&#39;s webapps</Menu.Item>
              <Menu.Menu position='right'>
                {/*<Menu.Item onClick={::this.toggleLoginModal}>Login</Menu.Item>*/}
                <Menu.Item>
                  {
                    authStore.userData ?
                      `@${authStore.userData.username}` :
                      <TelegramLoginButton dataSize='small' dataOnauth={ user => authStore.login(user) } botName={ BOT_USERNAME } />
                  }
                </Menu.Item>
              </Menu.Menu>
            </Container>
          </Menu>
          <Container style={{ padding: '4rem 0' }}>
            <Grid columns={2}>
              <Grid.Row>
                <Grid.Column width={12}>
                  <Route exact path='/' component={Index} />
                  <Route path='/lenta-comments' component={LentaComments} />
                  <Route path='/youtube-utils' component={YouTubeUtils} />
                </Grid.Column>
                <Grid.Column width={4}>
                  <Sidebar />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Container>
          <Modal open={this.state.loginModalIsOpen} onClose={::this.toggleLoginModal} basic size='small'>
            <Modal.Content>
            </Modal.Content>
          </Modal>
        </div>
      </Router>
    );
  }
}

export default Root;
