import React, { Component } from 'react';
import { Message } from 'semantic-ui-react';

class Index extends Component {
  render() {
    return (
      <div>
        <Message info>Please select the app from menu.</Message>
      </div>
    );
  }
}

export default Index;
